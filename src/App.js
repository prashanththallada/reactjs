import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Custom from './components/custom.js'

class App extends Component {
  render() {
    var a ={
      age: 20,
      array : ['ab','bs','c']
    }
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <Custom name={"Prashanth"} user={a}/>
        <Custom name={"Prashanth"} user={a}/>
        <Custom name={"Prashanth"} user={a}/>
        <Custom name={"Prashanth"} user={a}/>
      </div>
    );
  }
}

export default App;
