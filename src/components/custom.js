import React, { Component } from 'react';

class custom extends Component{
    constructor(props){
        super();
        this.state={
            age:props.user.age
        }
    }
    increaseinAge(){
        this.setState({
            age:this.state.age+3
        })
    }
    render(){
        return(
            <div>
                <p>A paragraph tag</p>
                <p>My name: {this.props.name}</p>
                <p>age:{this.state.age}</p>
                <p>array:{this.props.user.array.map((key)=><li>{key}</li>)}</p>
                <button onClick={this.increaseinAge.bind(this)}>Increase in age</button>
            </div>
        )
    }
}

export default custom;