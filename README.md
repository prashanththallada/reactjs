# ReactJS
A simple project on ReactJS 

## How to use

Open a command window in your required path.

Run npm install to install all the dependencies in the package.json file.

Run 'npm start' to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> npm start
```

Point your browser to `http://localhost:3000/explorer`. 